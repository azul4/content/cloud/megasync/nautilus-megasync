# nautilus-megasync

Upload your files to your Mega account from nautilus

https://mega.nz/linux/repo/Arch_Extra/x86_64/

<br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/cloud/megasync/nautilus-megasync.git
```

